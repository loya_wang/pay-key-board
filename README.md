# pay-key-board

> A Vue.js component

## Build Setup

``` bash
# install dependencies
仅支持vue2.0 版本

# 注册组件

import PayKeyBoard from 'pay-key-board'
Vue.component('pay-key-board', PayKeyBoard)

```
## 使用示例

``` bash
<pay-key-board :visible.sync='isShowBoard' :passWordlength='4' background-color = '#ee3f44'  pressKeyBgColor='red'   v-on:open="open" v-on:close="close" v-on:commit="commit">
  <template slot='prompt'>
    <p class="pay-forget">
      <label class="red">忘记密码？</label>
    </p> 
  </template>
</pay-key-board>

```
## arguments

``` bash
# visible[Boolean]          是否显示键盘
默认false，true:显示 false:不显示

# passWordlength[Number]    自定义密码长度 
默认6位

# background-color[String]  按钮颜色
默认值 #ee3f44

# pressKeyBgColor[String]   按下键盘数字的颜色
默认值 #f5f5f5

# commit[Function]
可接收一个参数：string。
将密码传给父组件

# open[Function]
打开键盘可接受一个回调方法

# close[Function]
关闭键盘可接受一个回调方法

# slot='prompt'
确认支付按钮与数字中间可插入自己想要的代码
eg:比如忘记密码等功能
```